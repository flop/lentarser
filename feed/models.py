from django.db import models


class Category(models.Model):
    name = models.CharField('Название', max_length=128)
    name_slug = models.SlugField('Слуг')
    pub_date = models.DateTimeField('Создана', auto_now_add=True)
    upd_date = models.DateTimeField('Редактирована', auto_now=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return '{}: {}'.format(self.name, self.pub_date)


class Data(models.Model):
    guid = models.CharField('Гуид', max_length=256)
    title = models.CharField('Заголовок', max_length=256)
    link = models.URLField('Урл')
    description = models.TextField('Описание')
    pub_date = models.DateTimeField('Дата')
    category = models.ForeignKey(Category, verbose_name='Категория')

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __str__(self):
        return '{} {}'.format(self.title, self.pub_date)
