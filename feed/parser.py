import urllib.request
from xml.dom import minidom
from datetime import datetime
# import models
from .models import Category, Data


LENTA_URL = 'https://lenta.ru/rss'


def get_lenta_items():
    url = urllib.request.urlopen(LENTA_URL)
    doc = minidom.parse(url)
    items = doc.getElementsByTagName("item")

    for i in items:
        _lenta_category = i.getElementsByTagName("category")[0]
        _guid = i.getElementsByTagName("guid")[0]
        _title = i.getElementsByTagName("title")[0]
        _link = i.getElementsByTagName("link")[0]
        _description = i.getElementsByTagName("description")[0]
        _pub_date = i.getElementsByTagName("pubDate")[0]
        # print(_lenta_category.firstChild.data)

        category, created = Category.objects.get_or_create(
            name=_lenta_category.firstChild.data)
        print(category)

        data = Data(
            guid=_guid.firstChild.data,
            title=_title.firstChild.data,
            link=_link.firstChild.data,
            description=_description.firstChild.data,
            pub_date=datetime.strptime(
                _pub_date.firstChild.data,
                "%a, %d %b %Y %H:%M:%S %z"),
            category=category
        )
        # data.save()
        print("save")
