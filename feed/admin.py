from django.contrib import admin
from .models import Category, Data


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'pub_date', 'upd_date']


@admin.register(Data)
class DataAdmin(admin.ModelAdmin):
    list_display = ['title', 'link', 'category']
