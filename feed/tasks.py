from celery.task import periodic_task
from celery.task.schedules import crontab
# import parser
from .parser import get_lenta_items


@periodic_task(
    run_every=30,
    name='task_get_items',
    ignore_result=True
)
def task_get_items():
    get_lenta_items()
